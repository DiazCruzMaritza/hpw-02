function TablaDeCalificaciones()
{
    var tabla_calificaciones = document.getElementById('tabla_calificaciones');

    var cuerpo_tabla = tabla_calificaciones.lastElementChild;


    var total = 0;

    var arreglo = [];


      for(var a = 0; a < cuerpo_tabla.children.length; a++)
      {

      total += Number(cuerpo_tabla.children[a].children[3].textContent);

      arreglo[a] = Number(cuerpo_tabla.children[a].children[3].textContent);

      }


    var tabla;


          for(var b = 0; b < arreglo.length-1; b++)
            {
                for(var c = 0; c < arreglo.length-b-1; c++)
              {
                if (arreglo[c+1] < arreglo[c]){

                      tabla = arreglo[c+1];

                      arreglo[c+1] = arreglo[c];

                      arreglo[c] = tabla;
                   }
               }
            }


    var parrafo_promedio = document.getElementById('parrafo_promedio');


    var parrafo_mayor_calificacion = document.getElementById('parrafo_mayor_calificacion');

    var mayor_calificacion = 0;

    var clave_parrafo_mayor_calificacion = "";

    var materia_parrafo_mayor_calificacion = "";

    var profesor_parrafo_mayor_calificacion = "";


    var parrafo_menor_calificacion = document.getElementById('parrafo_menor_calificacion');

    var menor_calificacion = 0;

    var clave_parrafo_menor_calificacion = "";

    var materia_parrafo_menor_calificacion = "";

    var profesor_parrafo_menor_calificacion = "";


    var filas = cuerpo_tabla.children.length;


      mayor_calificacion = arreglo [filas-1];

      menor_calificacion = arreglo[0];


    var promedio = total / cuerpo_tabla.children.length;


      for(var d = 0; d < cuerpo_tabla.children.length; d++)
      {
            if(mayor_calificacion == Number(cuerpo_tabla.children[d].children[3].textContent))
            {
                clave_parrafo_mayor_calificacion = cuerpo_tabla.children[d].children[0].textContent;

                materia_parrafo_mayor_calificacion = cuerpo_tabla.children[d].children[1].textContent;

                profesor_parrafo_mayor_calificacion = cuerpo_tabla.children[d].children[2].textContent;
            }

            if(menor_calificacion == Number(cuerpo_tabla.children[d].children[3].textContent))
            {
                clave_parrafo_menor_calificacion = cuerpo_tabla.children[d].children[0].textContent;

                materia_parrafo_menor_calificacion = cuerpo_tabla.children[d].children[1].textContent;

                profesor_parrafo_menor_calificacion = cuerpo_tabla.children[d].children[2].textContent;
            }
      }

      parrafo_promedio.textContent = "Promedio: " + promedio;

      parrafo_mayor_calificacion.textContent = "Materia con Mayor Calificacion: " + " CLAVE: " + clave_parrafo_mayor_calificacion + ". MATERIA: " + materia_parrafo_mayor_calificacion + ". PROFESOR(A): " + profesor_parrafo_mayor_calificacion + ". CALIFICACION: " + mayor_calificacion + ""; 

      parrafo_menor_calificacion.textContent = "Materia con Menor Calificacion: " + " CLAVE: " + clave_parrafo_menor_calificacion + ". MATERIA: " + materia_parrafo_menor_calificacion + ". PROFESOR(A): " + profesor_parrafo_menor_calificacion + ". CALIFICACION: " + menor_calificacion + ""; 
}


TablaDeCalificaciones();
